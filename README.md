# The project was terminated on 06.11.2024.
# DO NOT REPORT ANY ISSUES!

# Kernel module template ![alt text](https://gitlab.com/4neko/freebsd-kmod-template/-/raw/master/logo_600.png?ref_type=heads&inline=true)

A template for the FreeBSD kernel module.

A rust code is built as a static library which is then linked using standart FreeBSD kenrel module build system.

At the moment I still have not figured out how to correctly compile the Rust code and link it as ELF in order to avoid using FreeBSD Makefile and C.

## Usage

In order to build project, just exec in the root directory of the project:
```sh
make
```

In order to clean project dir exec:
```sh
make clean-all
```

For help exec:
```sh
make inform
```

In order to set custom libname (if you rename you crate) alter the 'LIB_NAME' in the Makefile.

## Optimization

By default the optimization is turned on:
```toml
overflow-checks = true
opt-level = "s"
lto = true
```

Opt-level is set to optimize by size. By default, the kernel module use O2 optimization, so the opt-level can be changed to 2.

Link-time optimisation is also enabled.

Overflow checks are enabled to, but in production it is better to have this option disabled, otherwise it will cause kernel panics.


